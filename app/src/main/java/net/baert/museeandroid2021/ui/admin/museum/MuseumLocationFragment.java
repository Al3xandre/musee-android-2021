package net.baert.museeandroid2021.ui.admin.museum;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.ui.user.myvisits.VisitViewModel;

import java.util.List;

public class MuseumLocationFragment extends Fragment {


    private VisitViewModel mVisitViewModel;

    private final OnMapReadyCallback callback = new OnMapReadyCallback() {

        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mVisitViewModel.getmCustomMuseum().observe(getViewLifecycleOwner(), customMuseum -> {
                LatLng museumLatLng = new LatLng(customMuseum.getMuseumLatLng().longitude, customMuseum.getMuseumLatLng().latitude);
                googleMap.addMarker(new MarkerOptions().position(museumLatLng).title(customMuseum.getMuseumName()).snippet("Nombre de visites: " + String.valueOf(customMuseum.getVisitNumber())));
            });

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        return inflater.inflate(R.layout.fragment_museum_location, container, false);
    }

    // debut bloque la rotation
    @Override
    public void onResume() {
        super.onResume();
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause() {
        super.onPause();
        requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }
    // fin bloque la rotation


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

        MuseumViewModel mMuseumViewModel = new ViewModelProvider(this).get(MuseumViewModel.class);
        mVisitViewModel = new ViewModelProvider(this).get(VisitViewModel.class);

        mMuseumViewModel.getmMuseumList().observe(getViewLifecycleOwner(), this::getVisits);
        mMuseumViewModel.retrieveMuseumList();
    }

    private void getVisits(List<Museum> museumList){
        museumList.forEach((museum -> {
            mVisitViewModel.getVisitByMuseumId(museum);
        }));
    }

}