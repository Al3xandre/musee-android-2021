package net.baert.museeandroid2021.ui.login;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String displayName;
    private String id;
    private String role;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(String displayName, String id, String role) {
        this.displayName = displayName;
        this.role = role;
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    String getDisplayName() {
        return displayName;
    }

    String getUserId() {
        return id;
    }
}