package net.baert.museeandroid2021.ui.user.stats;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import net.baert.museeandroid2021.data.model.Stats;
import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;

public class StatsViewModel extends ViewModel {

    private final MutableLiveData<Stats> mStats;

    private final StatsRepository statsRepository;


    public StatsViewModel() {
        this.statsRepository = StatsRepository.getInstance(new StatsDataSource());
        mStats = new MutableLiveData<>();
    }


    public MutableLiveData<Stats> getmStats() {
        return mStats;
    }

    public void retrieveStats(String userId) {
        statsRepository.getStatsByUserId(userId)
                .addOnSuccessListener(documentSnapshots -> {
                    Stats stats = documentSnapshots.getDocuments().get(0).toObject(Stats.class);
                    mStats.setValue(stats);
                }).addOnFailureListener(e -> Log.w("MyProfilViewModel", e.getMessage()));

    }
}
