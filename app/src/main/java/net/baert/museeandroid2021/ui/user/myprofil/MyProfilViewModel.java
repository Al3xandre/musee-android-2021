package net.baert.museeandroid2021.ui.user.myprofil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.model.User;
import net.baert.museeandroid2021.data.user.UserDataSource;
import net.baert.museeandroid2021.data.user.UserRepository;

public class MyProfilViewModel extends ViewModel {

    private MutableLiveData<Museum> mMuseum;
    private MutableLiveData<User> mUser;

    private final UserRepository userRepository;


    public MyProfilViewModel() {
        this.userRepository = UserRepository.getInstance(new UserDataSource());
        mMuseum = new MutableLiveData<>();
        mUser = new MutableLiveData<>();
    }

    public MutableLiveData<Museum> getmMuseum() {
        return mMuseum;
    }

    public MutableLiveData<User> getmUser() {
        return mUser;
    }

    public void retrieveUser(String userId) {
        userRepository.getUser(userId)
                .addOnSuccessListener(documentSnapshots -> {
                    User user = documentSnapshots.toObject(User.class);
                    mUser.setValue(user);
                }).addOnFailureListener(e -> {
                    Log.w("MyProfilViewModel", e.getMessage());
        });

    }

    public void retrieveUserAvatar(String userId) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference(userId);

        final long ONE_MEGABYTE = 1024 * 1024;
        storageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                if(bytes != null){
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    if(mUser.getValue() != null){
                        mUser.getValue().setAvatar(bitmap);
                        mUser.setValue(getmUser().getValue());
                    }
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }
}
