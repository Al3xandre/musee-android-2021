package net.baert.museeandroid2021.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import net.baert.museeandroid2021.MainActivity;
import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.login.LoginDataSource;
import net.baert.museeandroid2021.data.login.LoginRepository;
import net.baert.museeandroid2021.data.model.LoggedInUser;
import net.baert.museeandroid2021.ui.login.LoginActivity;

public class HomeActivity extends AppCompatActivity {

    private LoginRepository loginRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        loginRepository = LoginRepository.getInstance(new LoginDataSource());
        if (!getUserId().equals("")) {
            retrieveUser(getUserId());
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    // debut bloque la rotation
    @Override
    public void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause() {
        super.onPause();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }
    // fin bloque la rotation

    private void retrieveUser(String userId) {
        loginRepository.getUser(userId)
                .addOnSuccessListener(documentSnapshots -> {
                    LoggedInUser user = documentSnapshots.toObject(LoggedInUser.class);
                    if(user != null){
                        updateUiWithUser(user);
                    } else {
                        Intent intent = new Intent(this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }).addOnFailureListener(e -> {
            Log.w("MyProfilViewModel", e.getMessage());
        });

    }

    private String getUserId() {
        SharedPreferences sharedPref = getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }

    private void updateUiWithUser(LoggedInUser model) {
        if (model.getRole().compareTo("Undefined") == 0) {
            Toast.makeText(getApplicationContext(), "Erreur", Toast.LENGTH_SHORT).show();
        }
        if (model.getRole().compareTo("Admin") == 0) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("userId", model.getUserId());
            intent.putExtra("role", "Admin");
            startActivity(intent);
            finish();
        }
        if (model.getRole().compareTo("User") == 0) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("userId", model.getUserId());
            intent.putExtra("role", "User");
            startActivity(intent);
            finish();
        }
    }

}