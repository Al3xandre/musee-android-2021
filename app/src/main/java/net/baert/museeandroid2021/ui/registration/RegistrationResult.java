package net.baert.museeandroid2021.ui.registration;

import androidx.annotation.Nullable;

/**
 * Inscription result : success (user details) or error message.
 */
class RegistrationResult {
    @Nullable
    private RegisteredUserView success;
    @Nullable
    private Integer error;

    RegistrationResult(@Nullable Integer error) {
        this.error = error;
    }

    RegistrationResult(@Nullable RegisteredUserView success) {
        this.success = success;
    }

    @Nullable
    RegisteredUserView getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}