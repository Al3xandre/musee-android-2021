package net.baert.museeandroid2021.ui.user.stats;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Stats;

public class StatsFragment extends Fragment {



    private StatsViewModel statsViewModel;

    private TextView tvDistanceTotal, tvNombreVisits;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        this.statsViewModel = new ViewModelProvider(this).get(StatsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_stats, container, false);
        tvDistanceTotal = root.findViewById(R.id.tvDistanceTotal);
        tvNombreVisits = root.findViewById(R.id.tvNombreVisits);

        statsViewModel.retrieveStats(getUserId());
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        statsViewModel.getmStats().observe(getViewLifecycleOwner(), this::setStatsDisplay);
        return root;
    }

    @SuppressLint("SetTextI18n")
    private void setStatsDisplay(Stats stats){
        tvNombreVisits.setText(Integer.toString(stats.getNumberVisit()));
        tvDistanceTotal.setText(Float.toString(stats.getTravelledDistance()) + " km");
    }

    private String getUserId(){
        SharedPreferences sharedPref = requireContext().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }
}
