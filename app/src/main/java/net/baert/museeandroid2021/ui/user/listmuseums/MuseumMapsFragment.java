package net.baert.museeandroid2021.ui.user.listmuseums;

import android.Manifest;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import net.baert.museeandroid2021.R;

import java.util.List;

public class MuseumMapsFragment extends Fragment implements LocationListener {

    public static final int PERMISSION_GPS = 100;
    private LocationManager lm;
    private GoogleMap mgoogleMap;
    private MuseumViewModel mMuseumViewModel;
    private LatLng mMuseumCoordinates;

    private OnMapReadyCallback callback = new OnMapReadyCallback() {


        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mMuseumViewModel.getmLatLngList().observe(getViewLifecycleOwner(), new Observer<List<LatLng>>() {
                @Override
                public void onChanged(@Nullable List<LatLng> latLngList) {
                    Polyline polyline1 = mgoogleMap.addPolyline(new PolylineOptions()
                            .clickable(false).addAll(latLngList));
                    LatLng startPoint = new LatLng(latLngList.get(0).latitude, latLngList.get(0).longitude);
                    mgoogleMap.addMarker(new MarkerOptions().position(startPoint).title("Départ"));
                    LatLng endPoint = new LatLng(latLngList.get(latLngList.size()-1).latitude, latLngList.get(latLngList.size()-1).longitude);
                    mgoogleMap.addMarker(new MarkerOptions().position(endPoint).title("Arrivé"));
                    com.google.android.gms.maps.model.LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                    boundsBuilder.include(startPoint);
                    boundsBuilder.include(endPoint);

                    final LatLngBounds bounds = boundsBuilder.build();

                    mgoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                        @Override
                        public void onMapLoaded() {
                            mgoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                        }
                    });

                }
            });
            mgoogleMap = googleMap;

        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_museum_maps, container, false);

    }

    // debut bloque la rotation
    @Override
    public void onResume() {
        super.onResume();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
    }
    // fin bloque la rotation

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
// This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback backCallback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                goBack();
            }
        };

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), backCallback);

        Toast.makeText(getContext(), getString(R.string.visite_add_suxccess), Toast.LENGTH_SHORT).show();
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }

        mMuseumViewModel = new ViewModelProvider(this).get(MuseumViewModel.class);

        lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mMuseumCoordinates = new LatLng(bundle.getFloat("museumLat"),bundle.getFloat("museumLng"));
        }

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_GPS);
        } else {
            lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, MuseumMapsFragment.this, null);
        }


    }

    private void goBack() {
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager.beginTransaction().remove(this).commit();
        fragmentManager.popBackStack();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_GPS) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                lm.requestSingleUpdate(LocationManager.GPS_PROVIDER, MuseumMapsFragment.this, null);
            } else {
                Toast.makeText(getContext(), "Permission refusée", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mMuseumViewModel.getItinerary(mMuseumCoordinates, location);
        lm.removeUpdates(this);
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

}