package net.baert.museeandroid2021.ui.user.myprofil;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Visit;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class MuseumMyProfilViewHolder extends RecyclerView.ViewHolder {

    private final TextView tvMuseumName;
    private final TextView tvDate;

    public MuseumMyProfilViewHolder(@NonNull View itemView) {
        super(itemView);
        tvMuseumName = itemView.findViewById(R.id.museum_name_visits);
        tvDate = itemView.findViewById(R.id.date);

    }

    public void bind(Visit visit) {
        tvMuseumName.setText(visit.getMuseeName());
        SimpleDateFormat formatter = new SimpleDateFormat("dd / MM / yyyy HH:mm", Locale.getDefault());
        tvDate.setText(formatter.format(visit.getDateHeure().toDate()));
    }
}
