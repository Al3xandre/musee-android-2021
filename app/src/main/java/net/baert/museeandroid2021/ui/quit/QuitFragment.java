package net.baert.museeandroid2021.ui.quit;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import net.baert.museeandroid2021.R;

public class QuitFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        //remove logged user
        SharedPreferences preferences = requireActivity().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        preferences.edit().remove("userId").apply();
        requireActivity().finishAndRemoveTask();
        return inflater.inflate(R.layout.fragment_quit, container, false);

    }
}
