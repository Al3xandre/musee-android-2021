package net.baert.museeandroid2021.ui.registration;

/**
 * Class exposing authenticated user details to the UI.
 */
class RegisteredUserView {
    private final String email;
    private final String password;
    private final String role;
    private final String lastname;
    private final String firstname;
    public//... other data fields that may be accessible to the UI

    RegisteredUserView(String email, String password, String lastname, String firstname) {
        this.email = email;
        this.password = password;
        this.lastname = lastname;
        this.firstname = firstname;
        role = "User";
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }


    public String getRole() {
        return role;
    }
}