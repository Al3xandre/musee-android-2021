package net.baert.museeandroid2021.ui.admin.museum;


import com.google.android.gms.maps.model.LatLng;

public class MuseumView {

    private String museumName;
    private int visitNumber;
    private LatLng museumLatLng;

    public MuseumView(String museumName, int visitNumber, LatLng museumLatLng) {
        this.museumName = museumName;
        this.visitNumber = visitNumber;
        this.museumLatLng = museumLatLng;
    }

    public String getMuseumName() {
        return museumName;
    }

    public void setMuseumName(String museumName) {
        this.museumName = museumName;
    }

    public int getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(int visitNumber) {
        this.visitNumber = visitNumber;
    }

    public LatLng getMuseumLatLng() {
        return museumLatLng;
    }

    public void setMuseumLatLng(LatLng museumLatLng) {
        this.museumLatLng = museumLatLng;
    }
}
