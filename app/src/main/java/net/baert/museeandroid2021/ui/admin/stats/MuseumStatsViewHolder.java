package net.baert.museeandroid2021.ui.admin.stats;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;

public class MuseumStatsViewHolder extends RecyclerView.ViewHolder {

    private final TextView tvMuseumName;
    private final TextView tvMuseumDistance;
    private final TextView tvMuseumRate;


    public MuseumStatsViewHolder(@NonNull View itemView) {
        super(itemView);
        tvMuseumName = itemView.findViewById(R.id.museum_name_stats);
        tvMuseumDistance = itemView.findViewById(R.id.museum_distance);
        tvMuseumRate = itemView.findViewById(R.id.museum_rate);

    }

    public void bind(Museum museum) {
        Context context = this.itemView.getContext();
        tvMuseumName.setText(museum.getNom_du_musee());
        if( museum.getEvaluationNumber() > 0){
            tvMuseumRate.setText(context.getString(R.string.avg_rate,  museum.getTotalRate() / museum.getEvaluationNumber()));
        } else {
            tvMuseumRate.setText(context.getString(R.string.no_avg_rate));
        }
        if(museum.getTotalTravelledDistance() > 0){
        tvMuseumDistance.setText(context.getString(R.string.total_travelled_distance, museum.getTotalTravelledDistance()));
        } else {
            tvMuseumDistance.setText(context.getString(R.string.no_total_travelled_distance));
        }
    }
}
