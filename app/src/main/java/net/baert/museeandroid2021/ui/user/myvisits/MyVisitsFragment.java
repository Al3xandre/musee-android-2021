package net.baert.museeandroid2021.ui.user.myvisits;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.SnapshotParser;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Visit;

import java.util.Objects;

public class MyVisitsFragment extends Fragment {

    private RecyclerView rvVisits;
    private View root;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FirestorePagingAdapter<Visit, VisitsViewHolder> mAdapter;

    private final FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();
    private final CollectionReference mPostsCollection = mFirestore.collection("visits");
    private  Query mQuery ;
    private TextView tvNoVisit;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_my_visits, container, false);
        rvVisits = root.findViewById(R.id.ltVisits);
        tvNoVisit = root.findViewById(R.id.tv_empty_list_visit);
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayoutVisits);
        // Init mRecyclerView
        rvVisits.setHasFixedSize(true);
        rvVisits.setLayoutManager(new LinearLayoutManager(this.getContext()));

        mQuery = mPostsCollection.whereEqualTo("idUser", getUserId()).orderBy("dateHeure", Query.Direction.DESCENDING);

        setupAdapter();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.refresh();
            }
        });



        return root;
    }

    private void setupAdapter() {

        // Init Paging Configuration
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(20)
                .build();

        // Init Adapter Configuration
        FirestorePagingOptions<Visit> options = new FirestorePagingOptions.Builder<Visit>()
                .setLifecycleOwner(this)
                .setQuery(mQuery, config,  new SnapshotParser<Visit>() {
                    @NonNull
                    @Override
                    public Visit parseSnapshot(@NonNull DocumentSnapshot snapshot) {
                        return Objects.requireNonNull(snapshot.toObject(Visit.class));
                    }
                })
                .build();

        // Instantiate Paging Adapter
        mAdapter = new FirestorePagingAdapter<Visit, VisitsViewHolder>(options) {
            @NonNull
            @Override
            public VisitsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.visit_item, parent, false);
                return new VisitsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull VisitsViewHolder viewHolder, int i, @NonNull Visit visit) {
                // Bind to ViewHolder
                viewHolder.bind(visit);
            }

            @Override
            protected void onError(@NonNull Exception e) {
                super.onError(e);
                Log.e("ListVisitsFragment", e.getMessage());
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        Toast.makeText(
                                root.getContext(),
                                "Error Occurred!",
                                Toast.LENGTH_SHORT
                        ).show();

                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        if(getItemCount() == 0) {
                            tvNoVisit.setVisibility(View.VISIBLE);
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;
                }
            }


        };

        // Finally Set the Adapter to mRecyclerView
        rvVisits.setAdapter(mAdapter);

    }

    private String getUserId(){
        SharedPreferences sharedPref = getContext().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }
}
