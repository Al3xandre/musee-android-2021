package net.baert.museeandroid2021.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import net.baert.museeandroid2021.MainActivity;
import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.ui.user.myprofil.MyProfilViewModel;
import net.baert.museeandroid2021.ui.registration.RegistrationActivity;

public class LoginActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;
    private MyProfilViewModel myProfilViewModel;
    EditText usernameEditText;
    EditText passwordEditText;
    Button loginButton;
    Button registerButton;
    ProgressBar loadingProgressBar;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        myProfilViewModel = new ViewModelProvider(this).get(MyProfilViewModel.class);

        usernameEditText = findViewById(R.id.username);
        passwordEditText = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        registerButton= findViewById(R.id.register);
        loadingProgressBar = findViewById(R.id.loading);

        if(!getUserId().equals("")){
            loginViewModel.retrieveUser(getUserId());
        }



        loginViewModel.getLoginFormState().observe(this, loginFormState -> {
            if (loginFormState == null) {
                return;
            }
            loginButton.setEnabled(loginFormState.isDataValid());
            if (loginFormState.getUsernameError() != null) {
                usernameEditText.setError(getString(loginFormState.getUsernameError()));
            }
            if (loginFormState.getPasswordError() != null) {
                passwordEditText.setError(getString(loginFormState.getPasswordError()));
            }
        });

        loginViewModel.getLoginResult().observe(this, loginResult -> {
            if (loginResult == null) {
                return;
            }
            loadingProgressBar.setVisibility(View.GONE);
            if (loginResult.getError() != null) {
                showLoginFailed(loginResult.getError());
            }
            if (loginResult.getSuccess() != null) {
                SharedPreferences sharedPref = getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("userId", loginResult.getSuccess().getUserId());
                editor.commit();
                updateUiWithUser(loginResult.getSuccess());
                setResult(Activity.RESULT_OK);
                //Complete and destroy login activity once successful
                finish();
            }



        });


        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                loginViewModel.login(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
            return false;
        });

        registerButton.setOnClickListener(v -> {
            Intent intent = new Intent(this, RegistrationActivity.class);
            intent.putExtra("email",usernameEditText.getText().toString());
            startActivity(intent);
        });

        loginButton.setOnClickListener(v -> {
            loadingProgressBar.setVisibility(View.VISIBLE);
            loginViewModel.login(usernameEditText.getText().toString(),
                    passwordEditText.getText().toString());

        });
    }


    private String getUserId(){
        SharedPreferences sharedPref = getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }

    private void updateUiWithUser(LoggedInUserView model) {
        if(model.getRole().compareTo("Undefined")==0){
            Toast.makeText(getApplicationContext(), "Erreur", Toast.LENGTH_SHORT).show();
        }
        if(model.getRole().compareTo("Admin")==0){
            Intent intent = new Intent(this, MainActivity.class);//TODO : vue menu navigation drawer admin
            intent.putExtra("email",usernameEditText.getText().toString());
            intent.putExtra("userId",model.getUserId());
            intent.putExtra("role", "Admin");
            startActivity(intent);
        }
        if(model.getRole().compareTo("User")==0){
            Intent intent = new Intent(this, MainActivity.class);//TODO : vue menu navigation drawer user
            intent.putExtra("email",usernameEditText.getText().toString());
            intent.putExtra("userId",model.getUserId());
            intent.putExtra("role", "User");
            startActivity(intent);
        }
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }
}