package net.baert.museeandroid2021.ui.user.myprofil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.SnapshotParser;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.User;
import net.baert.museeandroid2021.data.model.Visit;

import java.util.Objects;

public class MyProfilFragment extends Fragment implements View.OnClickListener {


    private MyProfilViewModel myProfilViewModel;

    private ImageView ivAvatar;
    private TextView tvLastname, tvFirstname, tvEmail, tvPhoneNumber, tvCity;
    private RecyclerView rvVisitedMuseum;
    private View root;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FirestorePagingAdapter<Visit, MuseumMyProfilViewHolder> mAdapter;
    private final FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();

    private final CollectionReference mPostsCollection = mFirestore.collection("visits");
    private Query mQuery;
    private TextView tvNoVisit;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        this.myProfilViewModel = new ViewModelProvider(this).get(MyProfilViewModel.class);

        root = inflater.inflate(R.layout.fragment_my_profil, container, false);
        tvLastname = root.findViewById(R.id.tvLastame);
        tvFirstname = root.findViewById(R.id.tvFirstname);
        tvEmail = root.findViewById(R.id.tvEmail);
        tvPhoneNumber = root.findViewById(R.id.tvPhoneNumber);
        tvCity = root.findViewById(R.id.tvCity);
        tvNoVisit = root.findViewById(R.id.tvNoVisit);


        rvVisitedMuseum = root.findViewById(R.id.ltMuseumsMyProfile);

        mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayoutMyProfile);
        // Init mRecyclerView
        rvVisitedMuseum.setHasFixedSize(true);
        rvVisitedMuseum.setLayoutManager(new LinearLayoutManager(this.getContext()));
        mQuery = mPostsCollection.whereEqualTo("idUser", getUserId()).orderBy("dateHeure", Query.Direction.DESCENDING);
        setupAdapter();

        ivAvatar = root.findViewById(R.id.ivAvatar);

        myProfilViewModel.retrieveUser(getUserId());
        myProfilViewModel.retrieveUserAvatar(getUserId());

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };

        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        myProfilViewModel.getmUser().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(@Nullable User user) {
                setUserDisplay(user);
            }
        });


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.refresh();
            }
        });



        return root;
    }
    private void setUserDisplay(User user){
        tvLastname.setText(user.getLastname());
        tvFirstname.setText(user.getFirstname());
        tvEmail.setText(user.getEmail());
        tvPhoneNumber.setText(user.getPhoneNumber());
        tvCity.setText(user.getCity());
        ivAvatar.setImageBitmap(user.getAvatar());
    }

    private String getUserId(){
        SharedPreferences sharedPref = getContext().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }


    private void setupAdapter() {

        // Init Paging Configuration
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(20)
                .build();

        // Init Adapter Configuration
        FirestorePagingOptions<Visit> options = new FirestorePagingOptions.Builder<Visit>()
                .setLifecycleOwner(this)
                .setQuery(mQuery, config,  new SnapshotParser<Visit>() {
                    @NonNull
                    @Override
                    public Visit parseSnapshot(@NonNull DocumentSnapshot snapshot) {
                        return Objects.requireNonNull(snapshot.toObject(Visit.class));
                    }
                })
                .build();

        // Instantiate Paging Adapter
        mAdapter = new FirestorePagingAdapter<Visit, MuseumMyProfilViewHolder>(options) {
            @NonNull
            @Override
            public MuseumMyProfilViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.visit_item, parent, false);
                return new MuseumMyProfilViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull MuseumMyProfilViewHolder viewHolder, int i, @NonNull Visit visit) {
                // Bind to ViewHolder
                viewHolder.bind(visit);
            }

            @Override
            protected void onError(@NonNull Exception e) {
                super.onError(e);
                Log.e("MuseuMyProfileFragment", e.getMessage());
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        Toast.makeText(
                                root.getContext(),
                                "Error Occurred!",
                                Toast.LENGTH_SHORT
                        ).show();

                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        if(getItemCount() == 0) {
                            tvNoVisit.setVisibility(View.VISIBLE);
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;
                }
            }


        };

        // Finally Set the Adapter to mRecyclerView
        rvVisitedMuseum.setAdapter(mAdapter);

    }

    @Override
    public void onClick(View v) {

    }
}
