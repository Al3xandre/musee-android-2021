package net.baert.museeandroid2021.ui.registration;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import net.baert.museeandroid2021.R;

import java.io.ByteArrayOutputStream;

public class RegistrationActivity extends AppCompatActivity {

    private Button btnRegister;
    private ImageButton btnPhoto;
    private ImageView imageView;
    private byte[] imageBytes;
    private RegistrationViewModel registrationViewModel;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        this.registrationViewModel = new ViewModelProvider(this).get(RegistrationViewModel.class);
        imageBytes = null;
        TextView etEmail = findViewById(R.id.etEmail);
        TextView etPassword = findViewById(R.id.etPassword);
        TextView etConfPassword = findViewById(R.id.etConfPassword);
        TextView etFirstname = findViewById(R.id.etFirstname);
        TextView etLastname = findViewById(R.id.etLastname);
        TextView etPhoneNumber = findViewById(R.id.etPhoneNumber);
        TextView etCity = findViewById(R.id.etCity);
        CheckBox rbIsAdmin = findViewById(R.id.rbIsAdmin);

        registrationViewModel.getInscriptionResult().observe(this, registrationResult -> {
            if (registrationResult == null) {
                return;
            }
            if (registrationResult.getError() != null) {
                showRegistrationFailed(registrationResult.getError());
            }
            if (registrationResult.getSuccess() != null) {
                updateUiWithUser(registrationResult.getSuccess());
                setResult(Activity.RESULT_OK);

                //Complete and destroy login activity once successful
                finish();
            }

        });

        this.imageView = findViewById(R.id.ivAvatar);
        this.btnRegister = findViewById(R.id.btnRegister);
        this.btnRegister.setOnClickListener(c -> {
            this.registrationViewModel.registration(etEmail.getText().toString(),
                    etPassword.getText().toString(), etConfPassword.getText().toString(), etLastname.getText().toString(),
                    etFirstname.getText().toString(), etPhoneNumber.getText().toString(), etCity.getText().toString(), rbIsAdmin.isChecked(), imageBytes);
        });

        this.btnPhoto = findViewById(R.id.imageButton);
        btnPhoto.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
                {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
                }
                else
                {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
                }
            }
        });
    }

    private void updateUiWithUser(RegisteredUserView model) {
        String welcome = getString(R.string.welcome) + model.getFirstname() +" "+ model.getLastname() + " !";
        // TODO : initiate successful logged intata experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showRegistrationFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
            else
            {
                Toast.makeText(this, R.string.camera_permission_denied, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            Log.w("onActivityResult - photo", "photo took");
            imageView.setImageBitmap(photo);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            this.imageBytes = baos.toByteArray();

        }
    }
}