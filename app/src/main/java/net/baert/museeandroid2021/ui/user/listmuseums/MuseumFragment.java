package net.baert.museeandroid2021.ui.user.listmuseums;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.model.Visit;
import net.baert.museeandroid2021.ui.user.myvisits.VisitViewModel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class MuseumFragment extends Fragment implements LocationListener, View.OnClickListener {

    private MuseumViewModel mMuseumViewModel;
    private String museumId, userId, currentDateAndTime;
    private TextView tvMuseumName, tvDate, tvDistance, tvDuree;
    private Map<String, String> mMapDurationDistance;
    public static final int PERMISSION_GPS = 100;
    private LocationManager lm;
    private Museum mMuseum;
    private Button btnBack, btnSaveItinerary;
    private ProgressBar pbLoading;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View museumView = inflater.inflate(R.layout.museum_fragment, container, false);

        tvDate = museumView.findViewById(R.id.tvDateMuseum);
        tvMuseumName = museumView.findViewById(R.id.tvMuseumName);
        tvDistance = museumView.findViewById(R.id.tvDistance);
        tvDuree = museumView.findViewById(R.id.tvDuree);
        btnBack = museumView.findViewById(R.id.btnBack);
        btnSaveItinerary = museumView.findViewById(R.id.btnSaveItinerary);
        pbLoading = museumView.findViewById(R.id.loading);

        return museumView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        lm = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_GPS);
//            ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_GPS);
        } else {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, MuseumFragment.this);
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            museumId = bundle.getString("museumId", "");
        }

        mMuseumViewModel = new ViewModelProvider(this).get(MuseumViewModel.class);

        mMuseumViewModel.retrieveMuseum(museumId);

        btnBack.setOnClickListener(this);
        btnSaveItinerary.setOnClickListener(this);

        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                goBack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        mMuseumViewModel.getmMuseum().observe(getViewLifecycleOwner(),this::setMuseumDisplay);

        mMuseumViewModel.getmDurationDistance().observe(getViewLifecycleOwner(), this::setDurationDistance);

        mMuseumViewModel.getmResultMessage().observe(getViewLifecycleOwner(),this::displayToast);
    }

    private void goBack() {
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager.popBackStack();
    }


    private void setDurationDistance(Map<String, String> mapDurationDistance) {
        pbLoading.setVisibility(View.GONE);
        mMapDurationDistance = mapDurationDistance;
        tvDuree.setText(mapDurationDistance.get("duree"));
        tvDistance.setText(mapDurationDistance.get("distance") + " km");
    }

    private void setMuseumDisplay(Museum museum) {
        mMuseum = museum;
        SimpleDateFormat sdf = new SimpleDateFormat("dd / MM / yyyy HH:mm", Locale.getDefault());
        currentDateAndTime = sdf.format(new Date());
        tvMuseumName.setText(museum.getNom_du_musee());
        tvDate.setText(currentDateAndTime);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_GPS) {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 400,1,MuseumFragment.this);
            } else {
                Toast.makeText(getContext(), "Permission refusée", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mMuseumViewModel.getDistanceAndDurations(location);
        lm.removeUpdates(this);
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onClick(View v) {
            if(v.getId() == R.id.btnBack){
                goBack();
            }
            else if (v.getId() == R.id.btnSaveItinerary) {
                if(mMapDurationDistance != null && mMuseum != null) {
                    SharedPreferences sharedPref = getContext().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
                    userId = sharedPref.getString("userId", "");
                    mMuseumViewModel.addVisit(userId, mMuseum.getNom_du_musee(), mMuseum.getId(), currentDateAndTime, Float.parseFloat(mMapDurationDistance.get("distance")));
                    Bundle bundle=new Bundle();
                    bundle.putFloat("museumLat", mMuseum.getCoordonnees_finales().get(0));
                    bundle.putFloat("museumLng", mMuseum.getCoordonnees_finales().get(1));
                    FragmentManager fragmentManager = getParentFragmentManager();
                    fragmentManager.beginTransaction()
                            .setReorderingAllowed(true)
                            .replace(R.id.nav_host_fragment, MuseumMapsFragment.class, bundle)
                            .commit();
                }else {
                    Toast.makeText(getContext(), "Toutes les données ne sont pas réunies, veuillez réessayer plus tard", Toast.LENGTH_SHORT).show();
                }

            }
    }

    private void displayToast(int resId){
        Toast.makeText(getContext(), getString(resId), Toast.LENGTH_SHORT).show();
        if(resId == R.string.no_coordonnees) {
            btnSaveItinerary.setEnabled(false);
        }
    }

}