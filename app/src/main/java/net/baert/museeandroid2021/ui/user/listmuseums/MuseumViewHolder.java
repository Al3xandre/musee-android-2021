package net.baert.museeandroid2021.ui.user.listmuseums;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;

public class MuseumViewHolder extends RecyclerView.ViewHolder {

    private final TextView tvMuseumName;
    private final TextView tvMuseumAddress;
    private final TextView tvMuseumId;
    private MutableLiveData<String> mMuseumId;

    public MutableLiveData<String> getmMuseumId() {
        return mMuseumId;
    }

    public MuseumViewHolder(@NonNull View itemView) {
        super(itemView);
        tvMuseumName = itemView.findViewById(R.id.museum_name);
        tvMuseumAddress = itemView.findViewById(R.id.museum_adr);
        tvMuseumId = itemView.findViewById(R.id.museum_id);
        mMuseumId = new MutableLiveData<>();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.w("MuseumViewHolder", tvMuseumId.getText().toString());
                mMuseumId.setValue(tvMuseumId.getText().toString());
            }
        });
    }

    public void bind(Museum museum) {
        Context context = this.itemView.getContext();
        tvMuseumName.setText(museum.getNom_du_musee());
        tvMuseumId.setText(museum.getId());
        tvMuseumAddress.setText(context.getString(R.string.adr) + museum.getAdr() + context.getString(R.string.commma) + museum.getCp() + context.getString(R.string.commma) + museum.getVille());
    }
}
