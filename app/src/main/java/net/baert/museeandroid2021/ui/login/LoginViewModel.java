package net.baert.museeandroid2021.ui.login;

import android.util.Log;
import android.util.Patterns;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.login.LoginDataSource;
import net.baert.museeandroid2021.data.login.LoginRepository;
import net.baert.museeandroid2021.data.model.LoggedInUser;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private final LoginRepository loginRepository;
    private String role = "Undefined";

    public LoginViewModel() {
        this.loginRepository = LoginRepository.getInstance(new LoginDataSource());
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }

    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public String login(String username, String password) {
        // can be launched in a separate asynchronous job
        loginRepository.login(username, password).addOnCompleteListener(task -> {
            if (task.isSuccessful() && task.getResult().getDocuments().size() != 0) {
                LoggedInUser data = ((LoggedInUser) task.getResult().getDocuments().get(0).toObject(LoggedInUser.class));
                String storedPassword = data.getPassword().split(":")[2];
                String hashedPasswordToTest = loginRepository.convertToPBKDF2(password.toCharArray(), data.getPassword().split(":")[1]).split(":")[2];

                if(storedPassword.equals(hashedPasswordToTest)){
                    loginResult.setValue(new LoginResult(new LoggedInUserView(data.getEmail(), task.getResult().getDocuments().get(0).getId(), data.getRole())));
                } else {
                    loginResult.setValue(new LoginResult(R.string.login_failed));
                }
            } else {
                loginResult.setValue(new LoginResult(R.string.login_failed));
            }
        });
        return role;
    }

    public void setRole(String role){
        this.role = role;
    }

    public String getRole(){
        return role;
    }

    public void loginDataChanged(String username, String password) {
        if (!username.isEmpty() && !password.isEmpty()) {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    public void retrieveUser(String userId) {
        loginRepository.getUser(userId)
                .addOnSuccessListener(documentSnapshots -> {
                    LoggedInUser user = documentSnapshots.toObject(LoggedInUser.class);
                    loginResult.setValue(new LoginResult(new LoggedInUserView(user.getEmail(), documentSnapshots.getId(), user.getRole())));
                }).addOnFailureListener(e -> {
            Log.w("MyProfilViewModel", e.getMessage());
        });

    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null ;
    }
}