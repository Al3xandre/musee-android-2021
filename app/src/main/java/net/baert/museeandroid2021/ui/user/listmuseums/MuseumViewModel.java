package net.baert.museeandroid2021.ui.user.listmuseums;

import android.location.Location;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.museum.MuseumDataSource;
import net.baert.museeandroid2021.data.museum.MuseumRepository;
import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;
import net.baert.museeandroid2021.data.visit.VisitDataSource;
import net.baert.museeandroid2021.data.visit.VisitRepository;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MuseumViewModel extends ViewModel {
    private MutableLiveData<Museum> mMuseum;
    private MutableLiveData<Integer> mResultMessage;
    private MutableLiveData<Map<String, String>> mDurationDistance;
    private MutableLiveData<List<LatLng>> mLatLngList;

    private final MuseumRepository museumRepository;
    private final VisitRepository visitRepository;
    private final StatsRepository statsRepository;



    public MuseumViewModel() {
        this.museumRepository = MuseumRepository.getInstance(new MuseumDataSource());
        this.visitRepository = VisitRepository.getInstance(new VisitDataSource());
        this.statsRepository = StatsRepository.getInstance(new StatsDataSource());
        mResultMessage = new MutableLiveData<>();
        mMuseum = new MutableLiveData<>();
        mDurationDistance = new MutableLiveData<>();
        mLatLngList = new MutableLiveData<>();
    }

    public MutableLiveData<Museum> getmMuseum() {
        return mMuseum;
    }

    public MutableLiveData<List<LatLng>> getmLatLngList() {
        return mLatLngList;
    }

    public MutableLiveData<Map<String, String>> getmDurationDistance() {
        return mDurationDistance;
    }

    public MuseumRepository getMuseumRepository() {
        return museumRepository;
    }

    public MutableLiveData<Integer> getmResultMessage() {
        return mResultMessage;
    }

    public void addVisit(String userId, String museumName, String museumId, String strDate, float distanceKm) {
        visitRepository.addVisit(userId, museumName, museumId, strDate, distanceKm).addOnSuccessListener(documentReference -> {
            statsRepository.updateStatsByUser(userId, distanceKm).addOnSuccessListener(queryDocumentSnapshots -> {
                museumRepository.updateTravelledDistanceByMuseumId(museumId, distanceKm).addOnSuccessListener(documentSnapshot -> {
                    mResultMessage.setValue(R.string.visite_add_suxccess);
                }).addOnFailureListener(e -> {
                    mResultMessage.setValue(R.string.visite_add_error);
                    Log.w(this.getClass().getSimpleName(), e.getMessage());
                });
            }).addOnFailureListener(e -> {
                mResultMessage.setValue(R.string.visite_add_error);
                Log.w(this.getClass().getSimpleName(), e.getMessage());
            });
        }).addOnFailureListener(e -> {
            mResultMessage.setValue(R.string.visite_add_error);
            Log.w(this.getClass().getSimpleName(), e.getMessage());
        });
    }

    public void retrieveMuseum(String museumId) {
        museumRepository.getMuseumById(museumId)
                .addOnSuccessListener(documentSnapshots -> {
                    Museum museum = documentSnapshots.toObject(Museum.class);
                    museum.setId(documentSnapshots.getId());
                    mMuseum.setValue(museum);
                }).addOnFailureListener(e -> {
            Log.w("MyProfilViewModel", e.getMessage());
        });

    }

    public void getDistanceAndDurations(Location location){

        if(mMuseum.getValue() != null){
            if(mMuseum.getValue().getCoordonnees_finales() != null) {
                OkHttpClient client = new OkHttpClient();

                String jsonString = "{\"locations\":[[" + location.getLongitude() + "," + location.getLatitude() + "],[" + mMuseum.getValue().getCoordonnees_finales().get(1) + "," + mMuseum.getValue().getCoordonnees_finales().get(0)
                        + "]],\"destinations\":[1],\"metrics\":[\"distance\",\"duration\"],\"metricsStrings\":[\"dur\",\"dis\"],\"resolve_locations\":\"false\",\"sources\":[0],\"units\":\"km\"}";

                RequestBody body = RequestBody.create(
                        MediaType.parse("application/json"), jsonString);

                Request request = new Request.Builder()
                        .url("https://api.openrouteservice.org/v2/matrix/driving-car")
                        .addHeader("Authorization", "5b3ce3597851110001cf62487919222fcdab4f7692ad76ecafc96fb0")
                        .post(body)
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        final String webContent = response.body().string();

                        try {

                            JSONObject jsonObject = new JSONObject(webContent);
                            JSONArray durationsJsonArray = jsonObject.getJSONArray("durations");
                            JSONArray durationJsonArray = durationsJsonArray.getJSONArray(0);
                            float duration = durationJsonArray.getLong(0);
                            JSONArray distancesJsonArray = jsonObject.getJSONArray("distances");
                            JSONArray distanceJsonArray = distancesJsonArray.getJSONArray(0);
                            float distance = distanceJsonArray.getLong(0);
                            Map<String, String> mapDurationDistance = new HashMap<>();
                            mapDurationDistance.put("distance", Float.toString(distance));
                            mapDurationDistance.put("duree", splitToComponentTimesToString(duration));
                            mDurationDistance.postValue(mapDurationDistance);
                        } catch (Throwable t) {
                            Log.e("My App", "Could not parse malformed JSON: \"\"" + t.getMessage());
                            t.printStackTrace();
                        }

                    }
                });
            } else {
                mResultMessage.setValue(R.string.no_coordonnees);
                //TODO: mresultmessage toats
            }
        }

    }

    public void getItinerary(LatLng museumCoordinates, Location location) {

        OkHttpClient client = new OkHttpClient();
        String jsonString = "{\"coordinates\":[["+location.getLongitude()+","+location.getLatitude()+"],["+museumCoordinates.longitude+","+museumCoordinates.latitude+"]]}";

        RequestBody body = RequestBody.create(
                MediaType.parse("application/json"), jsonString);

        Request request = new Request.Builder()
                .url("https://api.openrouteservice.org/v2/directions/driving-car")
                .addHeader("Authorization", "5b3ce3597851110001cf62487919222fcdab4f7692ad76ecafc96fb0" )
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final String webContent = response.body().string();

                try {
                    JSONObject jsonObject = new JSONObject(webContent);
                    JSONArray routesJsonArray = jsonObject.getJSONArray("routes");
                    JSONObject firstRoutesJsonArray = routesJsonArray.getJSONObject(0);
                    String encodedPolyline = firstRoutesJsonArray.getString("geometry");

                    List<LatLng> latLngList = PolyUtil.decode(encodedPolyline);
                    mLatLngList.postValue(latLngList);



                } catch (Throwable t) {
                    Log.e("My App", "Could not parse malformed JSON: \"\"" + t.getMessage() );
                    t.printStackTrace();
                }

            }
        });
    }

    private String splitToComponentTimesToString(float seconds)
    {
        float  longVal = seconds;
        int hours = (int) longVal / 3600;
        int remainder = (int) longVal - hours * 3600;
        int mins = remainder / 60;
        remainder = remainder - mins * 60;
        int secs = remainder;
        return hours + " h " + mins + " m";
    }
}