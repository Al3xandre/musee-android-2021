package net.baert.museeandroid2021.ui.admin.stats;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.SnapshotParser;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.model.Visit;
import net.baert.museeandroid2021.ui.user.myvisits.VisitViewModel;

import java.util.List;

public class StatsFragment extends Fragment {

    private RecyclerView rvMuseum;
    private View root;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FirestorePagingAdapter<Museum, MuseumStatsViewHolder> mAdapter;
    private final FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();

    private final CollectionReference mPostsCollection = mFirestore.collection("museums");
    private final Query mQuery = mPostsCollection.orderBy("nom_du_musee", Query.Direction.DESCENDING);


    private VisitViewModel mVisitViewModel;
    private TextView tvTotalVisitNumber;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {


        root = inflater.inflate(R.layout.stats_fragment, container, false);
        rvMuseum = root.findViewById(R.id.ltMuseumsStats);
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        tvTotalVisitNumber = root.findViewById(R.id.tvTotalVisitNumber);
        mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayoutStats);
        // Init mRecyclerView
        rvMuseum.setHasFixedSize(true);
        rvMuseum.setLayoutManager(new LinearLayoutManager(this.getContext()));

        setupAdapter();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.refresh();
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        // TODO: Use the ViewModel
        mVisitViewModel = new ViewModelProvider(this).get(VisitViewModel.class);

        mVisitViewModel.getmVisitList().observe(getViewLifecycleOwner(), this::setDisplay);
        mVisitViewModel.getVisitList();
    }

    public void setDisplay(List<Visit> visitList){
        tvTotalVisitNumber.setText(getResources().getString(R.string.totalNumberVisit, visitList.size()));
    }


    private void setupAdapter() {

        // Init Paging Configuration
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(20)
                .build();

        // Init Adapter Configuration
        FirestorePagingOptions<Museum> options = new FirestorePagingOptions.Builder<Museum>()
                .setLifecycleOwner(this)
                .setQuery(mQuery, config,  new SnapshotParser<Museum>() {
                    @NonNull
                    @Override
                    public Museum parseSnapshot(@NonNull DocumentSnapshot snapshot) {
                        return snapshot.toObject(Museum.class);
                    }
                })
                .build();

        // Instantiate Paging Adapter
        mAdapter = new FirestorePagingAdapter<Museum, MuseumStatsViewHolder>(options) {
            @NonNull
            @Override
            public MuseumStatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.museum_stats_item, parent, false);
                return new MuseumStatsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull MuseumStatsViewHolder viewHolder, int i, @NonNull Museum museum) {
                // Bind to ViewHolder
                viewHolder.bind(museum);
            }

            @Override
            protected void onError(@NonNull Exception e) {
                super.onError(e);
                Log.e("ListMuseumsFragment", e.getMessage());
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        Toast.makeText(
                                root.getContext(),
                                "Error Occurred!",
                                Toast.LENGTH_SHORT
                        ).show();

                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;
                }
            }


        };

        // Finally Set the Adapter to mRecyclerView
        rvMuseum.setAdapter(mAdapter);

    }

}