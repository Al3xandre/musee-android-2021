package net.baert.museeandroid2021.ui.user.listmuseums;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.SnapshotParser;
import com.firebase.ui.firestore.paging.FirestorePagingAdapter;
import com.firebase.ui.firestore.paging.FirestorePagingOptions;
import com.firebase.ui.firestore.paging.LoadingState;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Museum;

public class ListMuseumsFragment extends Fragment {
    private RecyclerView rvEtudiants;
    private View root;
    private TextView tvNoMuseum;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private FirestorePagingAdapter<Museum, MuseumViewHolder> mAdapter;
    private final FirebaseFirestore mFirestore = FirebaseFirestore.getInstance();

    private final CollectionReference mPostsCollection = mFirestore.collection("museums");
    private final Query mQuery = mPostsCollection.orderBy("nom_du_musee", Query.Direction.DESCENDING);
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_list_museums, container, false);
        rvEtudiants = root.findViewById(R.id.ltMuseums);
        tvNoMuseum = root.findViewById(R.id.tv_empty_list_museum);

        mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);
        // Init mRecyclerView
        rvEtudiants.setHasFixedSize(true);
        rvEtudiants.setLayoutManager(new LinearLayoutManager(this.getContext()));

        setupAdapter();
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.refresh();
            }
        });

        return root;
    }

    private void changeActivity(String museumId) {
        Bundle bundle=new Bundle();
        bundle.putString("museumId", museumId);
        FragmentManager fragmentManager = getParentFragmentManager();
        fragmentManager.beginTransaction()
                .setReorderingAllowed(true)
                .addToBackStack("ListMuseum")
                .replace(R.id.nav_host_fragment, MuseumFragment.class, bundle)
                .commit();
    }

    private void setupAdapter() {

        // Init Paging Configuration
        PagedList.Config config = new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .setPageSize(20)
                .build();

        // Init Adapter Configuration
        FirestorePagingOptions<Museum> options = new FirestorePagingOptions.Builder<Museum>()
                .setLifecycleOwner(this)
                .setQuery(mQuery, config,  new SnapshotParser<Museum>() {
                    @NonNull
                    @Override
                    public Museum parseSnapshot(@NonNull DocumentSnapshot snapshot) {
                        Museum museum = snapshot.toObject(Museum.class);
                        museum.setId(snapshot.getId());
                        return museum;
                    }
                })
                .build();

        // Instantiate Paging Adapter
        mAdapter = new FirestorePagingAdapter<Museum, MuseumViewHolder>(options) {
            @NonNull
            @Override
            public MuseumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = getLayoutInflater().inflate(R.layout.museum_item, parent, false);
                MuseumViewHolder museumViewHolder = new MuseumViewHolder(view);
                museumViewHolder.getmMuseumId().observe(getViewLifecycleOwner(), new Observer<String>() {
                    @Override
                    public void onChanged(@Nullable String museumId) {
                        changeActivity(museumId);
                    }
                });
                return museumViewHolder;
            }

            @Override
            protected void onBindViewHolder(@NonNull MuseumViewHolder viewHolder, int i, @NonNull Museum museum) {
                // Bind to ViewHolder
                viewHolder.bind(museum);
            }

            @Override
            protected void onError(@NonNull Exception e) {
                super.onError(e);
                Log.e("ListMuseumsFragment", e.getMessage());
            }

            @Override
            protected void onLoadingStateChanged(@NonNull LoadingState state) {
                switch (state) {
                    case LOADING_INITIAL:
                    case LOADING_MORE:
                        mSwipeRefreshLayout.setRefreshing(true);
                        break;

                    case LOADED:
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case ERROR:
                        Toast.makeText(
                                root.getContext(),
                                "Error Occurred!",
                                Toast.LENGTH_SHORT
                        ).show();

                        mSwipeRefreshLayout.setRefreshing(false);
                        break;

                    case FINISHED:
                        if(getItemCount() == 0) {
                            tvNoMuseum.setVisibility(View.VISIBLE);
                        }
                        mSwipeRefreshLayout.setRefreshing(false);
                        break;
                }
            }


        };

        // Finally Set the Adapter to mRecyclerView
        rvEtudiants.setAdapter(mAdapter);

    }
}
