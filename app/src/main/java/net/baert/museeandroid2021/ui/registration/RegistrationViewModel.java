package net.baert.museeandroid2021.ui.registration;

import android.util.Log;
import android.util.Patterns;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.RegisteredUser;
import net.baert.museeandroid2021.data.registration.RegistrationDataSource;
import net.baert.museeandroid2021.data.registration.RegistrationRepository;
import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;

import java.util.regex.Pattern;

public class RegistrationViewModel extends ViewModel {

    private final MutableLiveData<RegistrationFormState> registrationFormState = new MutableLiveData<>();
    private final MutableLiveData<RegistrationResult> registrationResult = new MutableLiveData<>();
    private final RegistrationRepository registrationRepository;

    public RegistrationViewModel() {
        this.registrationRepository = RegistrationRepository.getInstance(new RegistrationDataSource());
    }

    LiveData<RegistrationFormState> getInscriptionFormState() {
        return registrationFormState;
    }

    LiveData<RegistrationResult> getInscriptionResult() {
        return registrationResult;
    }

    public void registration(String username, String password, String confirmPassword, String firstname,
                             String lastname, String phoneNumber, String city, boolean isAdmin, byte[] imageBytes) {
        if(!username.isEmpty() && !password.isEmpty() && !confirmPassword.isEmpty()
        && !firstname.isEmpty() && !lastname.isEmpty() && !phoneNumber.isEmpty() && !city.isEmpty()
        && imageBytes != null) {
            if(Pattern.matches("^(.*)@{1}(.*)(\\.){1}(.*)$", username)) {

                if (password.equals(confirmPassword)) {
                    registrationRepository.isInscrit(username).addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                        @Override
                        public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                            if (queryDocumentSnapshots.getDocuments().size() == 0) {
                                registrationRepository.register(username, password, firstname, lastname, phoneNumber, city, isAdmin)
                                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                            @Override
                                            public void onSuccess(DocumentReference documentReference) {
                                                Log.d(this.getClass().toString(), "DocumentSnapshot added with ID: " + documentReference.getId());
                                                documentReference.get().addOnSuccessListener(task -> {
                                                    RegisteredUser data = (RegisteredUser) task.toObject(RegisteredUser.class);
                                                    registrationResult.setValue(new RegistrationResult(new RegisteredUserView(data.getEmail(), data.getPassword(), data.getLastname(), data.getFirstname())));
                                                });
                                                createStatistiqueForUser(documentReference.getId());
                                                if (imageBytes != null) {
                                                    FirebaseStorage storage = FirebaseStorage.getInstance();
                                                    StorageReference storageRef = storage.getReference(documentReference.getId());
                                                    UploadTask uploadTask = storageRef.putBytes(imageBytes);
                                                    uploadTask.addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception exception) {
                                                            // Handle unsuccessful uploads
                                                        }
                                                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                                        @Override
                                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                            // taskSnapshot.getMetadata() contains file metadata such as size, content-type, etc.
                                                            // ...
                                                            Log.w("photo", taskSnapshot.getUploadSessionUri().getPath());
                                                        }
                                                    });
                                                }

                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w(this.getClass().toString(), "Error adding document", e);
                                            }
                                        });
                            } else {
                                registrationResult.setValue(new RegistrationResult(R.string.user_already_exists));
                            }
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(this.getClass().toString(), "Error adding document, user already exists", e);
                        }
                    });
                } else {

                    registrationResult.setValue(new RegistrationResult(R.string.passord_conf_error));
                }
            }else {
                registrationResult.setValue(new RegistrationResult(R.string.wrong_email_format));

            }
        } else {

            registrationResult.setValue(new RegistrationResult(R.string.empty_field));
        }

    }

    public void registrationDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            registrationFormState.setValue(new RegistrationFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            registrationFormState.setValue(new RegistrationFormState(null, R.string.invalid_password));
        } else {
            registrationFormState.setValue(new RegistrationFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }

    private void createStatistiqueForUser(String userId){
        StatsRepository statsRepository = StatsRepository.getInstance(new StatsDataSource());
        statsRepository.createStats(userId);

    }
}