package net.baert.museeandroid2021.ui.admin.stats;

import androidx.lifecycle.ViewModel;

import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;

public class StatsViewModel extends ViewModel {

    private final StatsRepository statsRepository;

    public StatsViewModel() {
        this.statsRepository = StatsRepository.getInstance(new StatsDataSource());
    }

}