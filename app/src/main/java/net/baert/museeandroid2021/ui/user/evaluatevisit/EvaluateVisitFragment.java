package net.baert.museeandroid2021.ui.user.evaluatevisit;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.model.Evaluation;
import net.baert.museeandroid2021.data.model.Visit;
import net.baert.museeandroid2021.ui.user.myvisits.VisitViewModel;

public class EvaluateVisitFragment extends Fragment {

    private EvaluateVisitViewModel evaluateVisitViewModel;
    private VisitViewModel visitViewModel;
    private Visit lastVisit;
    private TextView tvLastVisit, tvAlreadyRated, tvLastMuseum, tvNoVisit;
    private RatingBar rbVisit;
    private Button btnRate;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        evaluateVisitViewModel = new ViewModelProvider(this).get(EvaluateVisitViewModel.class);
        visitViewModel = new ViewModelProvider(this).get(VisitViewModel.class);

        View root = inflater.inflate(R.layout.fragment_evaluate_visit, container, false);

        tvLastVisit = root.findViewById(R.id.last_visit_txt);
        rbVisit = root.findViewById(R.id.ratingBar_visit);
        btnRate = root.findViewById(R.id.btn_rate);
        tvNoVisit = root.findViewById(R.id.tv_no_visit_evaluation);
        tvLastMuseum = root.findViewById(R.id.tv_dernier_musee);
        tvAlreadyRated = root.findViewById(R.id.tv_already_rated);
        btnRate.setVisibility(View.GONE);

        btnRate.setOnClickListener(v -> {
            evaluateVisitViewModel.addRate(getUserId(), lastVisit, rbVisit.getRating());
        });
        // This callback will only be called when MyFragment is at least Started.
        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed(){
            }
        };


        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
        evaluateVisitViewModel.getmLastVisit().observe(getViewLifecycleOwner(), this::setLastVisitDisplay);
        evaluateVisitViewModel.getLastVisit(getUserId());

        evaluateVisitViewModel.getmResultMessage().observe(getViewLifecycleOwner(), this::displayToast);

        return root;
    }

    private void setLastVisitDisplay(Visit visit){
        lastVisit = visit;
        tvLastVisit.setText(visit.getMuseeName());
        evaluateVisitViewModel.getmEvaluationByVisitId().observe(getViewLifecycleOwner(), this::setVisibilityOfBtnRate);
        evaluateVisitViewModel.getEvaluationByVisitId(visit.getId());
    }

    private void setVisibilityOfBtnRate(Evaluation evaluation) {
        if(evaluation != null){
            tvAlreadyRated.setVisibility(View.VISIBLE);
            btnRate.setVisibility(View.GONE);
            rbVisit.setRating(evaluation.getRate());
            rbVisit.setIsIndicator(true);
        } else {
            btnRate.setVisibility(View.VISIBLE);
        }
    }

    private void displayToast(int resId){
        if(resId == R.string.rate_added_success){
            btnRate.setVisibility(View.GONE);
            tvAlreadyRated.setVisibility(View.VISIBLE);
            rbVisit.setIsIndicator(true);
        } else if (resId == R.string.no_last_visit) {
            tvLastMuseum.setVisibility(View.GONE);
            tvNoVisit.setVisibility(View.VISIBLE);
        }
        Toast.makeText(requireContext(), getString(resId), Toast.LENGTH_SHORT).show();
    }

    private String getUserId(){
        SharedPreferences sharedPref = requireContext().getSharedPreferences("net.baert.museeandroid2021", Context.MODE_PRIVATE);
        return sharedPref.getString("userId", "");
    }
}
