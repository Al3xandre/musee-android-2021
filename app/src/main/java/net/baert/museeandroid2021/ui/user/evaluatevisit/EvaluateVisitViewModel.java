package net.baert.museeandroid2021.ui.user.evaluatevisit;

import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import net.baert.museeandroid2021.R;
import net.baert.museeandroid2021.data.evaluation.EvaluationDataSource;
import net.baert.museeandroid2021.data.evaluation.EvaluationRepository;
import net.baert.museeandroid2021.data.model.Evaluation;
import net.baert.museeandroid2021.data.model.Visit;
import net.baert.museeandroid2021.data.museum.MuseumDataSource;
import net.baert.museeandroid2021.data.museum.MuseumRepository;
import net.baert.museeandroid2021.data.visit.VisitDataSource;
import net.baert.museeandroid2021.data.visit.VisitRepository;

public class EvaluateVisitViewModel extends ViewModel {
    private final MutableLiveData<Integer> mResultMessage;
    private final MutableLiveData<Visit> mLastVisit;
    private final MutableLiveData<Evaluation> mEvaluationByVisitId;

    private final EvaluationRepository evaluationRepository;
    private final VisitRepository visitRepository;
    private final MuseumRepository museumRepository;

    public EvaluateVisitViewModel() {
        this.evaluationRepository = EvaluationRepository.getInstance(new EvaluationDataSource());
        this.visitRepository = VisitRepository.getInstance(new VisitDataSource());
        this.museumRepository = MuseumRepository.getInstance(new MuseumDataSource());
        mResultMessage = new MutableLiveData<>();
        mEvaluationByVisitId = new MutableLiveData<>();
        mLastVisit = new MutableLiveData<>();
    }

    public MutableLiveData<Visit> getmLastVisit() {
        return mLastVisit;
    }

    public LiveData<Integer> getmResultMessage() {
        return mResultMessage;
    }

    public void addRate(String userId, Visit visit, float rate) {
        evaluationRepository.addEvaluation(userId, visit.getId(), rate).addOnSuccessListener(documentReference -> {
            museumRepository.addRateByMuseumId(visit.getIdMusee(), rate).addOnSuccessListener(documentSnapshot -> {
                mResultMessage.setValue(R.string.rate_added_success);
            }).addOnFailureListener(e -> {
                mResultMessage.setValue(R.string.rate_added_error);
                Log.w(this.getClass().getSimpleName(), e.getMessage());
            });
        }).addOnFailureListener(e -> {
            mResultMessage.setValue(R.string.rate_added_error);
            Log.w(this.getClass().getSimpleName(), e.getMessage());
        });
    }


    public void getLastVisit(String userId) {
        visitRepository.getLastVisitByUser(userId).addOnSuccessListener(documentReference -> {
            if(documentReference.getDocuments().size() > 0){
                Visit visit = documentReference.getDocuments().get(0).toObject(Visit.class);
                visit.setId(documentReference.getDocuments().get(0).getId());
                mLastVisit.setValue(visit);
            } else {
                mResultMessage.setValue(R.string.no_last_visit);
            }
        }).addOnFailureListener(e -> {
            Log.w(this.getClass().getSimpleName(), e.getMessage());
        });
    }


    public void getEvaluationByVisitId(String visitId) {
        evaluationRepository.getEvaluationByVisitId(visitId).addOnSuccessListener(queryDocumentSnapshots -> {
            if(queryDocumentSnapshots.getDocuments().size() > 0) {
                mEvaluationByVisitId.setValue(queryDocumentSnapshots.getDocuments().get(0).toObject(Evaluation.class));
            } else {
                mEvaluationByVisitId.setValue(null);
            }
        }).addOnFailureListener(e -> {
            Log.w(this.getClass().getSimpleName(), e);
        });
    }

    public MutableLiveData<Evaluation> getmEvaluationByVisitId() {
        return mEvaluationByVisitId;
    }
}
