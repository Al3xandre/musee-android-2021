package net.baert.museeandroid2021.ui.admin.museum;

import android.location.Location;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;

import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.museum.MuseumDataSource;
import net.baert.museeandroid2021.data.museum.MuseumRepository;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MuseumViewModel extends ViewModel {
    private MutableLiveData<List<Museum>> mMuseumList;

    private final MuseumRepository museumRepository;



    public MuseumViewModel() {
        this.museumRepository = MuseumRepository.getInstance(new MuseumDataSource());
        mMuseumList = new MutableLiveData<>();
    }

    public MutableLiveData<List<Museum>> getmMuseumList() {
        return mMuseumList;
    }

    public void retrieveMuseumList() {
        museumRepository.getMuseumList()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Museum> museumList = queryDocumentSnapshots.toObjects(Museum.class);
                    mMuseumList.setValue(museumList);
                }).addOnFailureListener(e -> {
            Log.w("MuseumViewModel", e.getMessage());
        });

    }
}