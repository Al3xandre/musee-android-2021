package net.baert.museeandroid2021.ui.user.myvisits;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.google.android.gms.maps.model.LatLng;

import net.baert.museeandroid2021.data.model.Museum;
import net.baert.museeandroid2021.data.model.Visit;
import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;
import net.baert.museeandroid2021.data.visit.VisitDataSource;
import net.baert.museeandroid2021.data.visit.VisitRepository;
import net.baert.museeandroid2021.ui.admin.museum.MuseumView;

import java.util.List;

public class VisitViewModel extends ViewModel {
    private VisitRepository visitRepository;
    private StatsRepository statsRepository;

    private MutableLiveData<Visit> mVisit;
    private MutableLiveData<Integer> mResultMessage;
    private MutableLiveData<List<Visit>> mVisitList;
    private MutableLiveData<MuseumView> mCustomMuseum;

    public VisitViewModel() {
        this.statsRepository = StatsRepository.getInstance(new StatsDataSource());
        this.visitRepository = VisitRepository.getInstance(new VisitDataSource());
        mVisit = new MutableLiveData<>();
        mResultMessage = new MutableLiveData<>();
        mCustomMuseum = new MutableLiveData<>();
        mVisitList = new MutableLiveData<>();
    }

    public MutableLiveData<Visit> getmVisit() {
        return mVisit;
    }

    public MutableLiveData<List<Visit>> getmVisitList() {
        return mVisitList;
    }

    public MutableLiveData<MuseumView> getmCustomMuseum() {
        return mCustomMuseum;
    }



    public void getVisitList() {
        visitRepository.getVisitList().addOnSuccessListener(queryDocumentSnapshots -> {
            List<Visit> visitList = queryDocumentSnapshots.toObjects(Visit.class);
            mVisitList.setValue(visitList);
        }).addOnFailureListener(e -> {
            Log.w("L'enregistrement a échoué", e.getMessage());
        });
    }

    public void getVisitByMuseumId(Museum museum) {
        visitRepository.getVisitByMuseumId(museum.getId()).addOnSuccessListener(queryDocumentSnapshots -> {
            int visitNumber = queryDocumentSnapshots.getDocuments().size();
            if(museum.getCoordonnees_finales() != null){
                MuseumView museumView = new MuseumView(museum.getNom_du_musee(), visitNumber, new LatLng(museum.getCoordonnees_finales().get(1), museum.getCoordonnees_finales().get(0)));
                mCustomMuseum.setValue(museumView);
            }

        }).addOnFailureListener(e -> {
            Log.w("L'enregistrement a échoué", e.getMessage());
        });
    }
}