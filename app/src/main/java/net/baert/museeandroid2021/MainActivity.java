package net.baert.museeandroid2021;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.NavInflater;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import net.baert.museeandroid2021.data.model.Stats;
import net.baert.museeandroid2021.data.stats.StatsDataSource;
import net.baert.museeandroid2021.data.stats.StatsRepository;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private String userId, userRole;
    private TextView tvVisitNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        userId = getIntent().getStringExtra("userId");
        userRole = getIntent().getStringExtra("role");
        NavigationView navigationView = findViewById(R.id.nav_view);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        View headerView = navigationView.getHeaderView(0);
        tvVisitNumber = headerView.findViewById(R.id.tvVisitNumber);
        navigationView.getMenu().clear();


        getUserThumbnail(navigationView);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.



        NavHostFragment navHost = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
        NavController navController = navHost.getNavController();

        NavInflater navInflater = navController.getNavInflater();
        NavGraph graph = navInflater.inflate(R.navigation.mobile_navigation);

        if (userRole.equals("Admin")) {
            tvVisitNumber.setVisibility(View.GONE);
            graph.setStartDestination(R.id.nav_museum_location);
            navigationView.inflateMenu(R.menu.activity_main_drawer_admin);
        } else {
            getVisitNumber();
            navigationView.inflateMenu(R.menu.activity_main_drawer_user);
            graph.setStartDestination(R.id.nav_my_profil);
        }

        navController.setGraph(graph);
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                    R.id.nav_museum_location,
                    R.id.nav_stats_admin,
                    R.id.nav_my_profil,
                    R.id.nav_list_museums,
                    R.id.nav_my_visits,
                    R.id.nav_evaluate_visit,
                    R.id.nav_stats,
                    R.id.nav_quit
            )
                    .setDrawerLayout(drawer)
                    .build();


        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        getVisitNumber();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    private void getUserThumbnail(NavigationView navigationView){
        View headerView = navigationView.getHeaderView(0);
        ImageView imageViewMenu = headerView.findViewById(R.id.imageView);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference(userId);

        final long ONE_MEGABYTE = 1024 * 1024;
        storageRef.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                imageViewMenu.setImageBitmap(scaleDown(bitmap, 150, true));
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    private Bitmap scaleDown(Bitmap realImage, float maxImageSize, boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        if (ratio >= 1.0){ return realImage;}
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        return Bitmap.createScaledBitmap(realImage, width,
                height, filter);
    }

    public void getVisitNumber() {
        SharedPreferences sharedPref = getSharedPreferences("net.baert.museeandroid2021",Context.MODE_PRIVATE);
        String userId = sharedPref.getString("userId", "");
        StatsRepository statsRepository = StatsRepository.getInstance(new StatsDataSource());
        statsRepository.getStatsByUserId(userId)
                .addOnSuccessListener(queryDocumentSnapshots -> {

                    Stats stat = queryDocumentSnapshots.getDocuments().get(0).toObject(Stats.class);

                    NavigationView navigationView = findViewById(R.id.nav_view);
                    View headerView = navigationView.getHeaderView(0);
                    TextView tvVisitNumber = headerView.findViewById(R.id.tvVisitNumber);
                    String message = getResources().getString(R.string.visit_number) + ' '+ Integer.toString(stat.getNumberVisit());
                    tvVisitNumber.setText(message);
                });

    }
}