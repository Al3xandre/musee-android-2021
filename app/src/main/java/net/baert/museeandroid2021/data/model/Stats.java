package net.baert.museeandroid2021.data.model;

public class Stats {

    private int numberVisit;
    private String userId;
    private float travelledDistance;

    public Stats() {
    }

    public Stats(String userId) {
        this.numberVisit = 0;
        this.userId = userId;
        this.travelledDistance = 0;
    }

    public String getUserId() {
        return userId;
    }

    public float getTravelledDistance() {
        return travelledDistance;
    }

    public int getNumberVisit() {

        return numberVisit;
    }

    public void setNumberVisit(int numberVisit) {
        this.numberVisit = numberVisit;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setTravelledDistance(float travelledDistance) {
        this.travelledDistance = travelledDistance;
    }
}
