package net.baert.museeandroid2021.data.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.LoggedInUser;
import net.baert.museeandroid2021.data.Result;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class LoginRepository {

    private static volatile LoginRepository instance;

    private LoginDataSource dataSource;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private MutableLiveData<LoggedInUser> user = null;

    // private constructor : singleton access
    private LoginRepository(LoginDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static LoginRepository getInstance(LoginDataSource dataSource) {
        if (instance == null) {
            instance = new LoginRepository(dataSource);
        }
        return instance;
    }

    public Task<DocumentSnapshot> getUser(String userId) {
        return dataSource.getUser(userId);
    }

    public boolean isLoggedIn() {
        return user != null;
    }

    public void logout() {
        user = null;
        dataSource.logout();
    }

    public LiveData<LoggedInUser> getUser() {
        return user;
    }

    private void setLoggedInUser(LoggedInUser user) {
        this.user.setValue(user);
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    public Task<QuerySnapshot> login(String username, String password) {
        // handle login
        return dataSource.login(username, password);
    }

    public String convertToPBKDF2(char[] password, String salt)

    {
        // Get a random salt
        byte[] saltBytes = hexToString(salt);
        int PBKDF2_ITERATIONS = 1000;

        int HASH_BYTES = 24;
        // Hash the password
        byte[] hash = new byte[0];
        try {
            hash = generateKey(password, saltBytes, PBKDF2_ITERATIONS, HASH_BYTES);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
        // format iterations:salt:hash
        return PBKDF2_ITERATIONS + ":" + convertByteToHex(saltBytes) + ":" +  convertByteToHex(hash);

    }

    private byte[] generateKey(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return skf.generateSecret(spec).getEncoded();
    }

    private  String convertByteToHex(byte data[])
    {
        StringBuilder hexData = new StringBuilder();
        for (byte datum : data)
            hexData.append(Integer.toString((datum & 0xff) + 0x100, 16).substring(1));
        return hexData.toString();
    }

    private byte[] hexToString(String txtInHex)
    {
        byte[] val = new byte[txtInHex.length() / 2];

        for (int i = 0; i < val.length; i++) {
            int index = i * 2;
            int j = Integer.parseInt(txtInHex.substring(index, index + 2), 16);
            val[i] = (byte) j;
        }
        return val;
    }
}