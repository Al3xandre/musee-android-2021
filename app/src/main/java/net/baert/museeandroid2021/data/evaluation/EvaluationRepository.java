package net.baert.museeandroid2021.data.evaluation;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Evaluation;

public class EvaluationRepository {

    private static volatile EvaluationRepository instance;

    private final EvaluationDataSource dataSource;

    // private constructor : singleton access
    private EvaluationRepository(EvaluationDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static EvaluationRepository getInstance(EvaluationDataSource dataSource) {
        if (instance == null) {
            instance = new EvaluationRepository(dataSource);
        }
        return instance;
    }

    public Task<DocumentReference> addEvaluation(String userId, String visitId, float rate) {
        return dataSource.addEvaluation(new Evaluation(visitId,userId, rate));
    }

    public Task<QuerySnapshot> getEvaluationByVisitId(String visitId) {
        return dataSource.getEvaluationByVisitId(visitId);
    }
}
