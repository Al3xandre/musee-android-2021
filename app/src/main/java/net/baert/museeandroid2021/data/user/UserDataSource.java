package net.baert.museeandroid2021.data.user;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

public class UserDataSource {

    private FirebaseFirestore db;
    private final String collectionName = "users";

    public UserDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<DocumentSnapshot> getUser(String userId){
        return db.collection(collectionName).document(userId).get();
    }

}
