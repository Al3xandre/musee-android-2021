package net.baert.museeandroid2021.data.visit;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Visit;

public class VisitDataSource {

    private FirebaseFirestore db;
    private final String collectionName = "visits";

    public VisitDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<QuerySnapshot> getLastVisitMusee(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection(collectionName).whereEqualTo("idUser", userId).orderBy("dateHeure", Query.Direction.DESCENDING).limit(1).get();
    }
    public Task<QuerySnapshot> getVisitsMusee(String userId) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection(collectionName).whereEqualTo("user_id", userId).get();
    }
    public Task<DocumentReference> addEvaluation(Visit visit) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection(collectionName).add(visit);
    }

    public Task<DocumentReference> addVisit(Visit visit) {
        return db.collection(collectionName).add(visit);
    }

    public Task<QuerySnapshot> getVistByMuseumId(String museumId) {
        return db.collection(collectionName).whereEqualTo("idMusee", museumId).get();
    }

    public Task<QuerySnapshot> getVisitList() {
        return db.collection(collectionName).get();
    }
}