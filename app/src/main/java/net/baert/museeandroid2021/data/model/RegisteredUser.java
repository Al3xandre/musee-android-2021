package net.baert.museeandroid2021.data.model;

/**
 * Data class that captures user information for inscrit in users retrieved from InscriptionRepository
 */
public class RegisteredUser {

    private String email;
    private String password;
    private  String lastname;
    private  String firstname;
    private String phoneNumber;
    private String city;
    private String role;
    public RegisteredUser( String email, String password, String lastname, String firstname, String phoneNumber, String city, boolean isAdmin) {
        this.email = email;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phoneNumber = phoneNumber;
        this.city = city;
        this.role = isAdmin ? "Admin" : "User";
    }

    public RegisteredUser() {
    }

    public String getCity() {
        return city;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {return password;}

    public String getRole() {return role;}
}