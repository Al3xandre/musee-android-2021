package net.baert.museeandroid2021.data.model;

public class Evaluation {

    private String visitId;
    private Float rate;
    private String userId;

    public Evaluation(String visitId, String userId, Float r) {
        this.visitId = visitId;
        this.userId = userId;
        this.rate= r;
    }

    public Evaluation() {
    }


    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
