package net.baert.museeandroid2021.data.login;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.LoggedInUser;
import net.baert.museeandroid2021.data.Result;
import net.baert.museeandroid2021.data.model.User;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.Executor;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    FirebaseFirestore db ;


    public LoginDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<QuerySnapshot> login(String username, String password) {
        return db.collection("users").whereEqualTo("email", username)
                .get();
    }

    public void logout() {
        // TODO: revoke authentication
    }

    public Task<DocumentSnapshot> getUser(String userId){
        return db.collection("users").document(userId).get();
    }



}