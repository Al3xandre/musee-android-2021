package net.baert.museeandroid2021.data.model;

import com.google.firebase.firestore.DocumentId;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    @DocumentId
    private String userId;
    private String email;
    private String password;
    private String role;
    private int visitNumber;

    public LoggedInUser(String userId, String email) {
        this.userId = userId;
        this.email = email;
    }

    public LoggedInUser() {
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(int visitNumber) {
        this.visitNumber = visitNumber;
    }

    public String getPassword() {
        return password;
    }
    public String getUserId() {
        return userId;
    }

    public String getEmail() {
        return email;
    }
    public String getRole() { return role; }
}