package net.baert.museeandroid2021.data.model;

import com.google.firebase.firestore.DocumentId;

import java.util.List;

public class Museum {

    private float totalRate;
    private float totalTravelledDistance;
    private int EvaluationNumber;
    private String nom_du_musee;
    private String cp;
    private String ville;
    private String adr;
    @DocumentId
    private String id;
    private List<Float> coordonnees_finales;


    public Museum(String nom_du_musee, String cp, String ville, String adr, String id) {
        this.nom_du_musee = nom_du_musee;
        this.cp = cp;
        this.ville = ville;
        this.adr = adr;
        this.id = id;
    }

    public Museum() {
    }

    public List<Float> getCoordonnees_finales() {
        return coordonnees_finales;
    }

    public void setCoordonnees_finales(List<Float> coordonnees_finales) {
        this.coordonnees_finales = coordonnees_finales;
    }

    public float getTotalTravelledDistance() {
        return totalTravelledDistance;
    }

    public void setTotalTravelledDistance(float totalTravelledDistance) {
        this.totalTravelledDistance = totalTravelledDistance;
    }

    public float getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(float totalRate) {
        this.totalRate = totalRate;
    }

    public int getEvaluationNumber() {
        return EvaluationNumber;
    }

    public void setEvaluationNumber(int evaluationNumber) {
        EvaluationNumber = evaluationNumber;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public String getNom_du_musee() {
        return nom_du_musee;
    }

    public void setNom_du_musee(String nom_du_musee) {
        this.nom_du_musee = nom_du_musee;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
