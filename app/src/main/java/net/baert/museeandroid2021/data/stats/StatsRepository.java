package net.baert.museeandroid2021.data.stats;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Stats;
import net.baert.museeandroid2021.data.registration.RegistrationDataSource;
import net.baert.museeandroid2021.data.registration.RegistrationRepository;

public class StatsRepository {

    private static volatile StatsRepository instance;

    private StatsDataSource dataSource;

    private StatsRepository(StatsDataSource dataSource) {
        this.dataSource = new StatsDataSource();
    }

    public static StatsRepository getInstance(StatsDataSource dataSource) {
        if (instance == null) {
            instance = new StatsRepository(dataSource);
        }
        return instance;
    }


    public Task<QuerySnapshot> getStatsByUserId(String userId) {
        return dataSource.getStatsByUserId(userId);
    }

    public void createStats(String userId) {
        Stats stats = new Stats(userId);
        dataSource.createStats(stats);
    }

    public Task<QuerySnapshot> updateStatsByUser(String userId, float travelledDistance) {
        return dataSource.updateStatsByUser(userId, travelledDistance);
    }
}
