package net.baert.museeandroid2021.data.registration;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.Result;
import net.baert.museeandroid2021.data.model.RegisteredUser;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class RegistrationDataSource {

    private final String collectionName = "users";

    public Task<DocumentReference> register(RegisteredUser registeredUser) {
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            return db.collection(collectionName)
                    .add(registeredUser);
    }

    public Task<QuerySnapshot> getRegisteredUser(String username) {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        return db.collection("users").whereEqualTo("email", username).get();
    }

}