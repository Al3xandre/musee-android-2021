package net.baert.museeandroid2021.data.user;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.LoggedInUser;
import net.baert.museeandroid2021.data.registration.RegistrationDataSource;
import net.baert.museeandroid2021.data.registration.RegistrationRepository;

public class UserRepository {

    private static volatile UserRepository instance;

    private UserDataSource dataSource;

    // private constructor : singleton access
    private UserRepository(UserDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static UserRepository getInstance(UserDataSource dataSource) {
        if (instance == null) {
            instance = new UserRepository(dataSource);
        }
        return instance;
    }

    public Task<DocumentSnapshot> getUser(String userId) {
        return dataSource.getUser(userId);
    }

}
