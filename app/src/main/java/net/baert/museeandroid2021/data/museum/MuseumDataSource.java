package net.baert.museeandroid2021.data.museum;

import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Museum;

public class MuseumDataSource {

    private FirebaseFirestore db;
    private final String collectionName = "museums";

    public MuseumDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<DocumentSnapshot> getMuseumById(String museumId) {
        return db.collection(collectionName).document(museumId).get();
    }

    public Task<QuerySnapshot> getMuseumList() {
        return db.collection(collectionName).get();
    }

    public Task<DocumentSnapshot> addRateByMuseumId(String idMusee, float rate) {
        return db.collection(collectionName).document(idMusee).get().addOnSuccessListener(documentSnapshot -> {
            Museum museum = documentSnapshot.toObject(Museum.class);
            museum.setTotalRate( museum.getTotalRate() + rate);
            museum.setEvaluationNumber( museum.getEvaluationNumber() + 1);
            db.collection(collectionName).document(idMusee).set(museum);
        }).addOnFailureListener(e -> {
            Log.w(this.getClass().getSimpleName(), e.getMessage());
        });
    }

    public Task<DocumentSnapshot> updateTravelledDistanceByMuseumId(String museumId, float distanceKm) {
        return db.collection(collectionName).document(museumId).get().addOnSuccessListener(documentSnapshot -> {
            Museum museum = documentSnapshot.toObject(Museum.class);
            museum.setTotalTravelledDistance( museum.getTotalTravelledDistance() + distanceKm);
            db.collection(collectionName).document(museumId).set(museum);
        }).addOnFailureListener(e -> {
            Log.w(this.getClass().getSimpleName(), e.getMessage());
        });
    }
}
