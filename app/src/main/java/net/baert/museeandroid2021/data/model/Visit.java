package net.baert.museeandroid2021.data.model;

import android.provider.ContactsContract;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentId;

public class Visit {
    @DocumentId
    private String id;
    private String idMusee;
    private String museeName;
    private String idUser;
    private Timestamp dateHeure;
    private Float distance;

    public Visit(String idMusee, String museeName, String idUser, Timestamp dateHeure, Float distance) {
        this.idMusee = idMusee;
        this.museeName = museeName;
        this.idUser = idUser;
        this.dateHeure = dateHeure;
        this.distance = distance;
    }

    public Visit() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMusee() {
        return idMusee;
    }

    public void setIdMusee(String idMusee) {
        this.idMusee = idMusee;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public Timestamp getDateHeure() {
        return dateHeure;
    }

    public void setDateHeure(Timestamp dateHeure) {
        this.dateHeure = dateHeure;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public String getMuseeName() {
        return museeName;
    }

    public void setMuseeName(String museeName) {
        this.museeName = museeName;
    }
}
