package net.baert.museeandroid2021.data.evaluation;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Evaluation;

public class EvaluationDataSource {

    private FirebaseFirestore db;
    private final String collectionName = "evaluations";

    public EvaluationDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<DocumentReference> addEvaluation(Evaluation e) {
        return db.collection(collectionName).add(e);
    }

    public Task<QuerySnapshot> getEvaluationByVisitId(String visitId) {
        return db.collection(collectionName).whereEqualTo("visitId", visitId).get();
    }
}