package net.baert.museeandroid2021.data.museum;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.user.UserDataSource;
import net.baert.museeandroid2021.data.user.UserRepository;

public class MuseumRepository {

    private static volatile MuseumRepository instance;

    private MuseumDataSource dataSource;

    // private constructor : singleton access
    private MuseumRepository(MuseumDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static MuseumRepository getInstance(MuseumDataSource dataSource) {
        if (instance == null) {
            instance = new MuseumRepository(dataSource);
        }
        return instance;
    }

    public Task<DocumentSnapshot> getMuseumById(String museumId) {
        return dataSource.getMuseumById(museumId);
    }

    public Task<QuerySnapshot> getMuseumList() {
        return dataSource.getMuseumList();
    }

    public Task<DocumentSnapshot> addRateByMuseumId(String idMusee, float rate) {
        return dataSource.addRateByMuseumId(idMusee, rate);
    }

    public Task<DocumentSnapshot> updateTravelledDistanceByMuseumId(String museumId, float distancekm) {
        return dataSource.updateTravelledDistanceByMuseumId(museumId, distancekm);
    }
}
