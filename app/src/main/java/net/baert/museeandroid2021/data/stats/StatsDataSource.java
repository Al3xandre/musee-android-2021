package net.baert.museeandroid2021.data.stats;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Stats;

public class StatsDataSource {

    private FirebaseFirestore db;
    private final String collectionName = "statistiques";

    public StatsDataSource() {
        db = FirebaseFirestore.getInstance();
    }

    public Task<QuerySnapshot> getStatsByUserId(String userId) {
        return db.collection(collectionName).whereEqualTo("userId", userId).limit(1).get();
    }

    public void createStats(Stats stats) {
        db.collection(collectionName).add(stats);
    }

    public Task<QuerySnapshot> updateStatsByUser(String userId, float travelledDistance) {
        return db.collection(collectionName).whereEqualTo("userId", userId).get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    Stats stats = queryDocumentSnapshots.getDocuments().get(0).toObject(Stats.class);
                    stats.setNumberVisit(stats.getNumberVisit() + 1);
                    stats.setTravelledDistance(stats.getTravelledDistance() + travelledDistance);
                    db.collection(collectionName).document(queryDocumentSnapshots.getDocuments().get(0).getId()).set(stats);
                });
    }
}
