package net.baert.museeandroid2021.data.registration;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.LoggedInUser;
import net.baert.museeandroid2021.data.Result;
import net.baert.museeandroid2021.data.model.RegisteredUser;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of login status and user credentials information.
 */
public class RegistrationRepository {

    private static volatile RegistrationRepository instance;

    private RegistrationDataSource dataSource;

    // If user credentials will be cached in local storage, it is recommended it be encrypted
    // @see https://developer.android.com/training/articles/keystore
    private LoggedInUser user = null;

    // private constructor : singleton access
    private RegistrationRepository(RegistrationDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static RegistrationRepository getInstance(RegistrationDataSource dataSource) {
        if (instance == null) {
            instance = new RegistrationRepository(dataSource);
        }
        return instance;
    }

    public Task<QuerySnapshot> isInscrit(String username) {
        return dataSource.getRegisteredUser(username);
    }//TODO : Vérifier s'il est inscrit

    public Task<DocumentReference> register(String email, String password, String firstname, String lastname, String phoneNumber, String city, boolean isAdmin) {//TODO : inscrire
        // handle login
        return dataSource.register(new RegisteredUser(email, convertToPBKDF2(password.toCharArray()), firstname, lastname, phoneNumber, city, isAdmin));
    }


    private String convertToPBKDF2(char[] password)

    {
        // Generate a random salt
        SecureRandom random = new SecureRandom();
        int SALT_BYTES = 24;
        byte[] salt = new byte[SALT_BYTES];
        random.nextBytes(salt);
        int PBKDF2_ITERATIONS = 1000;

        int HASH_BYTES = 24;
        // Hash the password
        byte[] hash = new byte[0];
        try {
            hash = generateKey(password, salt, PBKDF2_ITERATIONS, HASH_BYTES);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
            return null;
        }
        // format iterations:salt:hash
        return PBKDF2_ITERATIONS + ":" + convertByteToHex(salt) + ":" +  convertByteToHex(hash);

    }

    private byte[] generateKey(char[] password, byte[] salt, int iterations, int bytes)
            throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, bytes * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return skf.generateSecret(spec).getEncoded();
    }

    private  String convertByteToHex(byte data[])
    {
        StringBuilder hexData = new StringBuilder();
        for (byte datum : data)
            hexData.append(Integer.toString((datum & 0xff) + 0x100, 16).substring(1));
        return hexData.toString();
    }
}