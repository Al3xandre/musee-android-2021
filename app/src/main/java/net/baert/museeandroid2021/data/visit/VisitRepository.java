package net.baert.museeandroid2021.data.visit;

import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.QuerySnapshot;

import net.baert.museeandroid2021.data.model.Visit;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VisitRepository {

    private static volatile VisitRepository instance;
    private VisitDataSource dataSource;

    // private constructor : singleton access
    private VisitRepository(VisitDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public static VisitRepository getInstance(VisitDataSource dataSource) {
        if (instance == null) {
            instance = new VisitRepository(dataSource);
        }
        return instance;
    }

    public Task<DocumentReference> addVisit(String userId, String museumName, String museumId, String strDate, float distanceKm) {

        DateFormat formatter = new SimpleDateFormat("dd / MM / yyyy HH:mm", Locale.getDefault());
        Date date = null;
        try {
            date = (Date) formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dataSource.addVisit(new Visit(museumId, museumName, userId, new Timestamp(date), distanceKm));
    }

    public Task<QuerySnapshot> getLastVisitByUser(String userId) {
        return dataSource.getLastVisitMusee(userId);
    }

    public Task<DocumentReference> addEvaluationList(Visit v) {
        return dataSource.addEvaluation(v);
    }

    public Task<QuerySnapshot> getVisitByMuseumId(String museumId) {
        return dataSource.getVistByMuseumId(museumId);
    }

    public Task<QuerySnapshot> getVisitList() {
        return dataSource.getVisitList();
    }
}
